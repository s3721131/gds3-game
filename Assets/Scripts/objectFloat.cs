﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectFloat : MonoBehaviour
{
    public float amplitude;
    float number_1;
    float number_2;
    float number_3;
    public float frequency;
    public float degreesPerSecond;

    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    void Start()
    {
        number_1 = Random.Range(0.1f, 1f);
        number_2 = Random.Range(0.25f, 0.75f);
        number_3 = Random.Range(-10f, 10f);
        posOffset = transform.position;
        frequency = number_1;
        amplitude = number_2;
        degreesPerSecond = number_3;
    }

    void Update()
    {
        // Make object move up and down, and spin
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        transform.position = tempPos;
    }
}
