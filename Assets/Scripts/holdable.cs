﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class holdable : MonoBehaviour
{
    public bool isHeld = false;
    public bool isActivated = false;
    public Color onColor;
    public Color offColor;
    private Vector3 startPos;
    public Shader shaderBase;
    public Shader shaderHeld;
    Renderer rend;

    void Start()
    {
        shaderBase = Shader.Find("Diffuse");
        startPos = gameObject.transform.position;
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        if (gameObject.transform.position.y < -15)
        {
            gameObject.transform.position = startPos;
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        if (isHeld)
        {
            rend.material.shader = shaderHeld;
        }
        else
        {
            rend.material.shader = shaderBase;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        // If collision object is Button, change the colour of the button to be the same as self
        if (other.gameObject.tag == "Button")
        {
            Debug.Log("Button Pressed");
            transform.GetComponent<Renderer>().material.color = onColor;
            other.transform.GetComponent<Renderer>().material.color = onColor;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Button")
        {
            Debug.Log("Button No Longer Pressed");
            transform.GetComponent<Renderer>().material.color = offColor;
            other.transform.GetComponent<Renderer>().material.color = offColor;
        }
    }
}
