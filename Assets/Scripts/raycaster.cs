﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class raycaster : MonoBehaviour
{
    public GameObject itemPos;
    bool holding = false;
    RaycastHit storage;
    RaycastHit hit;
    holdable holdableScript;
    Rigidbody rb;
    //textManager currentText;
    //public TMP_Text UIText;
    //bool click = false;
    //float timeLeft = 4.0f;
    //public bool gameClose = false;
    public GameObject overlay;

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit))
        {
            // Hold Object if Raycasted Object is "Holdable"
            if (Input.GetMouseButtonDown(0) && hit.transform.tag == "Holdables" && holding == false && hit.distance < 5)
            {
                holding = true;
                storage = hit;
                hit.transform.position = itemPos.transform.position;
                hit.transform.rotation = itemPos.transform.rotation;
                rb = hit.transform.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                rb.detectCollisions = false;
                overlay.SetActive(true);
            }
        }
        // Continue Holding Object if Mouse Held Down
        if (Input.GetMouseButton(0) && holding == true)
        {
            // Keep Object Momentum Set to Zero
            storage.transform.position = itemPos.transform.position;
            storage.transform.rotation = itemPos.transform.rotation;
            storage.rigidbody.velocity = Vector3.zero;

            // Adjust Script of Held Object to Signal it is currently being held
            holdableScript = storage.transform.GetComponent<holdable>();
            holdableScript.isHeld = true;

        }
        else
        {
            if (holding == true)
            {
                // Adjust Script of Held Object to Signal it is no longer being held
                holdableScript.isHeld = false;
                storage.rigidbody.isKinematic = false;
                rb.isKinematic = false;
                rb.detectCollisions = true;
                overlay.SetActive(false);
            }
            holding = false;
        }
    }

}
