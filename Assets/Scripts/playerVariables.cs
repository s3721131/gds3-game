﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerVariables : MonoBehaviour
{
    private bool canTeleport = true;

    void Start()
    {
        // In case of a teleport issue, every 5 seconds, allow player to teleport again regardless of portal status.
        InvokeRepeating("Teleport", 0f, 5.0f);
    }
    
    private void Teleport()
    {
        canTeleport = true;
    }
    public void SetTeleport(bool teleport)
    {
        canTeleport = teleport;
    }
    public bool GetTeleport()
    {
        return canTeleport;
    }
}
