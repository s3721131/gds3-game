﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class respawn : MonoBehaviour
{
    public GameObject teleportTo;
    public GameObject collideWith;
    public GameObject fadeElement;
    private bool playerIsOverlapping;
    private bool teleportNow;
    public float fadeSpeed = 10f;
    raycaster rc;
    FirstPersonDrifter fp;
    private Color objectColor;

    void Start()
    {
        objectColor = fadeElement.GetComponent<Image>().color;
        fp = collideWith.GetComponent<FirstPersonDrifter>();
        rc = collideWith.GetComponent<raycaster>();
    }

    void Update()
    {
        // If Overlapping, fade in, teleport player, then fade out
        if (playerIsOverlapping)
        {
            if (objectColor.a < 1)
            {
                float fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                fadeElement.GetComponent<Image>().color = objectColor;
            }
        }
        else if (teleportNow)
        {
            collideWith.transform.position = teleportTo.transform.position;
            teleportNow = false;
        }
        else if (!playerIsOverlapping && !teleportNow)
        {
            if (objectColor.a > 0)
            {
                float fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                fadeElement.GetComponent<Image>().color = objectColor;
                Debug.Log(objectColor.a);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // Disable movement briefly after collision
        if (other.tag == "Player")
        {
            playerIsOverlapping = true;
            StartCoroutine(TurnOffScript(0.2f));
            StartCoroutine(TurnOnScript(1.1f));
            StartCoroutine(WaitBeforeTeleport(1f));
            Debug.Log("Player Overlapped");
        }

    }

    private IEnumerator WaitBeforeTeleport(float time)
    {
        yield return new WaitForSeconds(time);
        teleportNow = true;
        playerIsOverlapping = false;
    }
    private IEnumerator TurnOnScript(float time)
    {
        yield return new WaitForSeconds(time);
        fp.enabled = true;
    }
    private IEnumerator TurnOffScript(float time)
    {
        yield return new WaitForSeconds(time);
        fp.enabled = false;
    }
}
