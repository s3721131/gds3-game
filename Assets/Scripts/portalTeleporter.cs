﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalTeleporter : MonoBehaviour
{
    public Transform player;
    public Transform receiver;
    public GameObject playerPrefab;
    FirstPersonDrifter fp;
    playerVariables pv;

    private bool playerIsOverlapping = false;
    // Update is called once per frame
    void Start()
    {
        fp = playerPrefab.GetComponent<FirstPersonDrifter>();
        pv = playerPrefab.GetComponent<playerVariables>();
    }
    void Update()
    {
        if (playerIsOverlapping)
        {
            if (pv.GetTeleport())
            {
                Vector3 portalToPlayer = player.position - transform.position;
                float rotationDiff = -Quaternion.Angle(transform.rotation, receiver.rotation);
                rotationDiff += 180;

                // Calculate Position Offset from Center of Render Plane
                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                positionOffset[0] = -positionOffset[0];
                // Debug.Log(positionOffset);

                fp.enabled = false;
                // Disable First Person Movement Temporarily
                player.position = receiver.position + positionOffset;
                playerIsOverlapping = false;

                // Turn First Person Movement Back On
                StartCoroutine(TurnOnScript(0.02f));

                // Cannot Teleport For Now
                pv.SetTeleport(false);
                StartCoroutine(TurnOnTeleporting(1f));


            }
                
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Player")
        {
            playerIsOverlapping = true;
        }

    }

    void OnTriggerExit (Collider other)
    {
        if (other.tag == "Player")
        {
            playerIsOverlapping = false;
        }

    }
    private IEnumerator TurnOnScript(float time)
    {
        yield return new WaitForSeconds(time);
        fp.enabled = true;
    }
    private IEnumerator TurnOnTeleporting(float time)
    {
        yield return new WaitForSeconds(time);
        pv.SetTeleport(true);
    }
}

