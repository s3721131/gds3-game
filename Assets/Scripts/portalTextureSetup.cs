﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalTextureSetup : MonoBehaviour
{
    public Camera cameraA;
    public Camera cameraB;
    public Camera cameraC;
    public Camera cameraCEnd;
    public Camera cameraD;
    public Camera cameraE;
    public Camera cameraF;

    public Material cameraMatA;
    public Material cameraMatB;
    public Material cameraMatC;
    public Material cameraMatCEnd;
    public Material cameraMatD;
    public Material cameraMatE;
    public Material cameraMatF;

    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 144;
    }

    
    void Start()
    {
        // Apply all textures to materials.
        if (cameraB.targetTexture != null)
        {
            cameraB.targetTexture.Release();
        }
        cameraB.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatB.mainTexture = cameraB.targetTexture;

        if (cameraA.targetTexture != null)
        {
            cameraA.targetTexture.Release();
        }
        cameraA.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatA.mainTexture = cameraA.targetTexture;

        if (cameraC.targetTexture != null)
        {
            cameraC.targetTexture.Release();
        }
        cameraC.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatC.mainTexture = cameraC.targetTexture;

        if (cameraD.targetTexture != null)
        {
            cameraD.targetTexture.Release();
        }
        cameraD.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatD.mainTexture = cameraD.targetTexture;

        if (cameraE.targetTexture != null)
        {
            cameraE.targetTexture.Release();
        }
        cameraE.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatE.mainTexture = cameraE.targetTexture;

        if (cameraF.targetTexture != null)
        {
            cameraF.targetTexture.Release();
        }
        cameraF.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatF.mainTexture = cameraF.targetTexture;

        if (cameraCEnd.targetTexture != null)
        {
            cameraCEnd.targetTexture.Release();
        }
        cameraCEnd.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMatCEnd.mainTexture = cameraCEnd.targetTexture;
    }
}

