﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectSpin : MonoBehaviour
{
    public float x = 1;
    public float spinDir = 1;

    void Update()
    {
        // Spin Object
        if (spinDir == 1)
        { 
        transform.Rotate(0, 0, x * Time.deltaTime);
        }
        if (spinDir == 2)
        {
        transform.Rotate(0, x, 0 * Time.deltaTime);
        }
    }
}
