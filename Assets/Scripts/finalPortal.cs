﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finalPortal : MonoBehaviour
{
    public GameObject button_1;
    public GameObject button_2;
    public GameObject button_3;
    public Material mat;
    public Material original;
    importantButton scr_1;
    importantButton scr_2;
    importantButton scr_3;
    public GameObject cable;

    public GameObject[] triggerables;
    public bool isActive = false;

    private bool portalActive = false;

    void Start()
    {
        scr_1 = button_1.transform.GetComponent<importantButton>();
        scr_2 = button_2.transform.GetComponent<importantButton>();
        scr_3 = button_3.transform.GetComponent<importantButton>();
        original = button_1.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (scr_1.isActive && scr_2.isActive && scr_3.isActive)
        {
            // If all buttons are active, activate the portal
            if (!portalActive)
            {
                button_1.GetComponent<MeshRenderer>().material = mat;
                button_2.GetComponent<MeshRenderer>().material = mat;
                button_3.GetComponent<MeshRenderer>().material = mat;
                for (int t = 0; t < triggerables.Length; t++)
                {
                    triggerables[t].SetActive(!triggerables[t].activeInHierarchy);
                }
                portalActive = true;
                cable.SetActive(!cable.activeInHierarchy);
            }
        }
        else if (portalActive)
        {
            button_1.GetComponent<MeshRenderer>().material = original;
            button_2.GetComponent<MeshRenderer>().material = original;
            button_3.GetComponent<MeshRenderer>().material = original;
            for (int t = 0; t < triggerables.Length; t++)
            {
                triggerables[t].SetActive(!triggerables[t].activeInHierarchy);
            }
            portalActive = false;
            cable.SetActive(!cable.activeInHierarchy);
        }
    }
}
