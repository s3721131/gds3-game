﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class importantButton : MonoBehaviour
{
    public GameObject[] cables;
    public GameObject[] triggerables;
    public int currentCable = 0;
    private Color baseColor;
    holdable hold;
    public bool isActive = false;

    void Start()
    {
        baseColor = transform.GetComponent<Renderer>().material.color;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Holdables")
        {
            isActive = true;
            hold = other.transform.GetComponent<holdable>();
            // For every 'cable', change the colour of the object to match cube/current colour.
            for (int t = 0; t < cables.Length; t++)
            {
                cables[t].transform.GetComponent<Renderer>().material.color = hold.onColor;
            }
            // For every 'triggerable', enable/disable the object
            for (int t = 0; t < triggerables.Length; t++)
            {
                triggerables[t].SetActive(!triggerables[t].activeInHierarchy);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Holdables")
        {
            isActive = false;
            hold = other.transform.GetComponent<holdable>();
            for (int t = 0; t < cables.Length; t++)
            { 
                cables[t].transform.GetComponent<Renderer>().material.color = hold.offColor;
            }

            for (int t = 0; t < triggerables.Length; t++)
            {
                triggerables[t].SetActive(!triggerables[t].activeInHierarchy);
            }
        }
    }
}
